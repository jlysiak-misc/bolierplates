from importlib import resources
from unittest import TestCase
from zipfile import ZipFile


class Test1( TestCase ):

    def test_resourcesExample1( self ):
        with resources.path( 'PKG.data', 'test_data_1.txt' ) as file_path:
            print( "resourceExample1:", file_path )
            with open( file_path ) as f:
                data = f.read().strip()
        self.assertEqual( data, "1 2 3", "test_data_1 doesn't contain: 1 2 3" )

    def test_resourcesExample2( self ):
        with resources.path( 'PKG.data', 'test_data_2.zip' ) as file_path:
            print( "resourceExample2:", file_path )
            with ZipFile( file_path ) as zipfile:
                print( "zipfile:", zipfile )
                with zipfile.open( 'test_data_2.txt' ) as f:
                    data = f.read().strip().decode( 'utf-8' )
        pattern = '\n'.join( [ '%d' % i for i in range( 1, 10 ** 5 + 1 ) ] )
        self.assertEqual( pattern, data, "data is not numbers from 1 to 10^5" )
