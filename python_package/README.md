# Python package bolierplate

Some code I usually start with when creating a Python package.

What it contains is:
- README file :wink:
- `.gitignore`
- `.style.yapf` for yapf (linked to `setup.cfg`)
- `pylint.cfg`
- `setup.py` with package CLI entry point
- package with `data` and `test` examples
    - using `unittest` module
    - using `importlib.resources` to grab files from the package
- simple `Makefile`
- `TODO.md`, hehe :wink:
- Super-duper simple file modification detector, that triggers unit test runner!
