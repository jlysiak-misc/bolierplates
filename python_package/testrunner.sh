#!/bin/sh

# Copyright (C) 2021, Jacek Łysiak
#
# Poor Man's unit tests runner.
# Just watches for file events in a given directory tree and run all
# unit tests when some file has been saved.
# I really recommend to go through manpages of flock, inotifywait and trap 
# briefly, but I will try to give you some explanations here.

# Directory to watch
DIRTREE=PKG
# Lock-file name
LOCKFILE=.test_runner_lock

# Clean up function triggered when this shell script exits
function cleanup {
    echo "Cleaning up..."
    # Obtain exclusive lock and drop it before executing `rm`
    flock $LOCKFILE rm $LOCKFILE
}

# Run `cleanup` when shell exits.
trap cleanup EXIT

# Execute a loop until there is something to read in the `while` loop process
# input, which is fed from substituted process. First `<` means redirection
# from file, and `<( #code )` process substitution. Bash execute the code and
# creates "temporary file" (let's say) containing stuff printed by the code,
# and this file if further redirected to `while` loop process stdin.
while read LINE
do
    # Ouch, some file has been modified!

    # Try to obtain exclusive access to lock-file in non-blocking manner ( -n )
    # If we got an exclusive access, we succeed and run `make test`.
    # Otherwise, `flock -n 9` fails and sub-shell (only!) exits.
    # We, of course, run sub-shell in background, so we are not blocking the
    # main loop.
    # This construct is interesting, as before we start sub-shell,
    # LOCKFILE is created and it's linked with sub-shell's file descriptor `9`
    # by redirecting fd 9's content to the file. Then `flock -n 9` means that 
    # we are trying to get an exclusive lock to the file opened under fd 9.
    ( flock -n 9 || exit 1; printf "\n\n"; make test ) 9> $LOCKFILE &

# Here it comes, inotifywait is app using Linux inotify interface to detect
# file events. `-m` is monitor, so the app runs all the time and listen for
# events. `-r` looks recursively for files in the `DIRTREE`. `-q` (like quiet)
# prints only file events happened, `-e` tells that we want only be informed
# about `CLOSE_WRITE` events (so when we closed a file and it was opened
# for writing) and `--format` is set to print just a path to file that
# generated event.
done < <(inotifywait -m -r $DIRTREE -q -e close_write --format '%w%f' )
