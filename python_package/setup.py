from setuptools import setup

setup(
    name='PKG',
    version='0.1',
    py_modules=[ 'PKG' ],
    install_requires=[],
    entry_points={ 'console_scripts': [ 'PKGcli = PKG:main' ]},
)
